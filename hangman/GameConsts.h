#pragma once

#include <string>

//dimensions in 2D that are whole numbers
struct Dim2Di
{
	int x, y;
};

//dimensions in 2D that are floating point numbers
struct Dim2Df
{
	float x, y;
};

 
/*
A box to put Games Constants in.
These are special numbers with important meanings (screen width, 
ascii code for the escape key, number of lives a player starts with,
the name of the title screen music track, etc.
*/
namespace GC
{
	//game play related constants to tweak
	const Dim2Di SCREEN_RES{ 1200,800 };

	const int MAX_WORDS = 1000;		//how many words, of each length, will we extract from the data
	const char ESCAPE_KEY{ 27 }, SPACE{ 32 }, UNDERSCORE{ 95 }, ENTER{ 13 }; //ascii codes
	const int MIN_WORD_LENGTH = 4, MAX_WORD_LENGTH = 13; //limits on any particular word
	const int WORD_LENGTHS = 10;	//how many different groups of words shall we have, each group are all the same length
	const int MAX_CHARS = 10 * 1000 * 1000; //how much data can we hold
	const int MAX_HINTS = 3;		//how many hints are we allowed
	const int NUM_ALPHABET = 27;	
	const int FIRST_HINT = 3;		//level at which we get a hint bonus
	const int NEXT_HINT = 2;		//every subsequent number of levels thereafter
	enum class GameMode { INTRO, PLAY, LEVEL_WON, LOST, WON };
	const Dim2Df HPOS{ 750,300 }, HSCALE{ 1,1 }; //for displaying the gallows
	const int START_SCORE = 25;		//initial score
	const int BADLETTER_SCORE = -2;	
	const int GOODLETTER_SCORE = 2;
	const int NEWLEVEL_SCORE = 2;


}


