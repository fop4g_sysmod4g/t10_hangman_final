#include <sstream>
#include <fstream>
#include <assert.h>

#include "Game.h"
#include "RandomUtils.h"

using namespace std;

/*
Sometimes you want an image to fit a certain percentage of the screen and you 
don't care what resolution the screen is or how big the texture is.
screenRes - res of screen
desScale - we want the texture to be this percentage of the screen res in size
texSize - how big the texture actually is
*/
sf::Vector2f Rescale(const Dim2Di& screenRes, const Dim2Df& desScale, const sf::IntRect& texSize)
{
	Dim2Df alpha{ screenRes.x * desScale.x, screenRes.y*desScale.y };
	alpha = Dim2Df{ alpha.x / texSize.width, alpha.y / texSize.height };
	sf::Vector2f res(alpha.x, alpha.y);
	return res;
}

//reset each word group
void InitialiseText(TextLib& tlib)
{
	for (int i = 0; i < GC::WORD_LENGTHS; ++i)
	{
		tlib.wordGrps[i].nWords = 0;
		tlib.wordGrps[i].length = GC::MIN_WORD_LENGTH + i;
	}
}

//load in the huge texture file
bool LoadText(TextLib& tlib, const string& file)
{
	fstream inFile;
	inFile.open(file, ios::in);
	if (!inFile)
		assert(false);
	if (inFile.bad())
		assert(false);
	if (inFile.fail())
		assert(false);

	inFile.seekg(0, ios_base::end);
	unsigned int size = (unsigned int)inFile.tellg();
	inFile.seekg(0, ios_base::beg);

	inFile.read(tlib.data, static_cast<std::streamsize>(size));
	tlib.numChars = static_cast<unsigned int>(inFile.gcount());
	if (tlib.numChars == 0)
		assert(false);

	inFile.close();
	return true;
}


//find a space with a letter after it
void FindStartLetter(TextLib& tlib, unsigned char& letter, int& wIdx, int& wStartIdx, unsigned char buff[], unsigned int& idx)
{
	bool found = false;
	while (idx < (tlib.numChars - 1) && !found)
	{
		if (isalpha((unsigned char)tlib.data[idx]) && (idx == 0 || !isalpha((unsigned char)tlib.data[idx - 1])))
			found = true;
		else
			idx++;
	}

	if (found)
	{
		letter = tlib.data[idx];
		//start a new word
		letter = toupper(letter);
		wIdx = 0;
		wStartIdx = idx;
		buff[wIdx++] = letter;
	}
}

//word complete, can we use it?
void WordComplete(TextLib& tlib, int& wIdx, int& wStartIdx)
{

	if (wIdx > GC::MAX_WORD_LENGTH || wIdx < GC::MIN_WORD_LENGTH)
	{
		//bad length so forget it
		wStartIdx = -1;
		wIdx = 0;
	}
	else
	{
		//save this word
		TextGrp& t = tlib.wordGrps[wIdx - GC::MIN_WORD_LENGTH];
		if (t.nWords < GC::MAX_WORDS)
			t.startIds[t.nWords++] = wStartIdx;
		wStartIdx = -1;
		wIdx = 0;
	}
}

//start building up one word, a letter at a time
void ProcessOneLetter(unsigned char& letter, int& wIdx, int& wStartIdx, unsigned char buff[])
{
	if (wIdx > GC::MAX_WORD_LENGTH)
	{
		//too long forget it
		wStartIdx = -1;
		wIdx = 0;
	}
	else
		buff[wIdx++] = letter;
}

/*
Go through the huge data buffer one character at a time
1. Look for a non-alpha character with an alpha character after it - assume this is the start of a word
2. Collect each following alpha character
3. As soon as you hit a non-alpha character then the word must be complete
4. See if the word is a good size and save it in the appropriate text group
*/
bool ParseText(TextLib& tlib)
{
	unsigned char buff[GC::MAX_WORD_LENGTH + 1];
	int wIdx = -1;
	int wStartIdx = -1;
	//check every character
	for (unsigned int idx = 0; idx < tlib.numChars; ++idx)
	{
		unsigned char c = tlib.data[idx];
		bool isLetter = isalpha(c);

		if (wStartIdx == -1)
		{
			FindStartLetter(tlib, c, wIdx, wStartIdx, buff, idx);
		}
		else if (wStartIdx != -1 && !isLetter)
		{
			WordComplete(tlib, wIdx, wStartIdx);
		}
		else if (isLetter)
		{
			ProcessOneLetter(c,wIdx,wStartIdx,buff);
		}
	}
	return true;
}

//draw the hanged man, but only enough to match how many fails we've had
void DrawGallows(sf::RenderWindow& window, GameData& gdata)
{
	int n = gdata.playerGuess.numFails;

	sf::RectangleShape wood;
	if (n > 0)
	{
		wood.setTexture(&gdata.wood);
		wood.setSize(sf::Vector2f(250 * GC::HSCALE.x, 25 * GC::HSCALE.y));
		wood.setPosition(GC::HPOS.x + 10 * GC::HSCALE.x, GC::HPOS.y + 10 * GC::HSCALE.y);
		window.draw(wood);
		wood.setSize(sf::Vector2f(25 * GC::HSCALE.x, 400 * GC::HSCALE.y));
		wood.setPosition(GC::HPOS.x + 230 * GC::HSCALE.x, GC::HPOS.y + 30 * GC::HSCALE.y);
		window.draw(wood);
		wood.setSize(sf::Vector2f(250 * GC::HSCALE.x, 25 * GC::HSCALE.y));
		wood.setPosition(GC::HPOS.x + 130 * GC::HSCALE.x, GC::HPOS.y + 410 * GC::HSCALE.y);
		window.draw(wood);
	}

	if (n > 7)
	{
		wood.setSize(sf::Vector2f(10 * GC::HSCALE.x, 50 * GC::HSCALE.y));
		wood.setPosition(GC::HPOS.x + 75 * GC::HSCALE.x, GC::HPOS.y + 30 * GC::HSCALE.y);
		window.draw(wood);
	}

	if (n > 1)
	{
		sf::CircleShape head;
		head.setRadius(30 * GC::HSCALE.x);
		head.setPosition(GC::HPOS.x + 50 * GC::HSCALE.x, GC::HPOS.y + 50 * GC::HSCALE.y);
		head.setFillColor(sf::Color::White);
		if (n > 7)
			head.setTexture(&gdata.deadHead);
		else
			head.setTexture(&gdata.head);
		window.draw(head);
	}
	//torso
	sf::RectangleShape body;
	if (n > 2)
	{
		body.setFillColor(sf::Color::Yellow);
		body.setPosition(GC::HPOS.x + 70 * GC::HSCALE.x, GC::HPOS.y + 110 * GC::HSCALE.y);
		body.setSize(sf::Vector2f(15 * GC::HSCALE.x, 100 * GC::HSCALE.y));
		window.draw(body);
	}
	//left arm
	if (n > 3)
	{
		body.setRotation(30);
		body.setPosition(GC::HPOS.x + 30 * GC::HSCALE.x, GC::HPOS.y + 130 * GC::HSCALE.y);
		body.setSize(sf::Vector2f(50 * GC::HSCALE.x, 15 * GC::HSCALE.y));
		window.draw(body);
	}
	//right arm
	if (n > 4)
	{
		body.setRotation(-30);
		body.setPosition(GC::HPOS.x + 80 * GC::HSCALE.x, GC::HPOS.y + 160 * GC::HSCALE.y);
		body.setSize(sf::Vector2f(50 * GC::HSCALE.x, 15 * GC::HSCALE.y));
		window.draw(body);
	}
	//left leg
	if (n > 5)
	{
		body.setRotation(-30);
		body.setPosition(GC::HPOS.x + 20 * GC::HSCALE.x, GC::HPOS.y + 240 * GC::HSCALE.y);
		body.setSize(sf::Vector2f(50 * GC::HSCALE.x, 15 * GC::HSCALE.y));
		window.draw(body);
	}
	//right leg
	if (n > 6)
	{
		body.setRotation(30);
		body.setPosition(GC::HPOS.x + 90 * GC::HSCALE.x, GC::HPOS.y + 220 * GC::HSCALE.y);
		body.setSize(sf::Vector2f(50 * GC::HSCALE.x, 15 * GC::HSCALE.y));
		window.draw(body);
	}
}

/*
The level of the game indicates how long a word to choose (longer equals harder)
So find the text group and then pick a word at random and copy it out of the 
huge data buffer.
*/
string PickRandomWord(TextLib& tlib, int level)
{
	assert(level >= 0 && level < GC::WORD_LENGTHS);
	TextGrp& t = tlib.wordGrps[level];
	int id = GetRandomRange(0, t.nWords);
	id = t.startIds[id];
	string tgt;
	for (int i = 0; i < t.length; ++i)
		tgt += toupper(tlib.data[id++]);
	return tgt;
}

//A new level or start of the game, so reset the player guess and find a new word
void Reset(GameData& gdata)
{
	gdata.playerGuess.numTries = 0;
	gdata.playerGuess.numFails = 0;
	for (int i = 0; i < GC::MAX_WORD_LENGTH; ++i)
		gdata.playerGuess.word[i] = GC::UNDERSCORE;
	for (int i = 0; i < GC::NUM_ALPHABET; ++i)
		gdata.playerGuess.fails[i] = 0;
	gdata.curWord = PickRandomWord(gdata.tlib, gdata.level);
}

//variables need carefully resetting when a level is complete
//plus we need to check for the game ending
void NextLevel(GameData& gdata)
{
	if (gdata.level < (GC::WORD_LENGTHS - 1))
	{
		gdata.mode = GC::GameMode::PLAY;
		gdata.level++;
		gdata.score += gdata.level * GC::NEWLEVEL_SCORE;
		if (gdata.level == gdata.playerGuess.nextHintLevel)
		{
			if (gdata.playerGuess.hints < GC::MAX_HINTS)
				gdata.playerGuess.hints++;
			gdata.playerGuess.nextHintLevel += GC::NEXT_HINT;
		}
		Reset(gdata);
	}
	else
		gdata.mode = GC::GameMode::WON;
}

//run once at the start of a new game
void Initialise(GameData& gdata)
{
	if (!gdata.wood.loadFromFile("data/wood_seamless.jpg"))
		assert(false);
	if (!gdata.head.loadFromFile("data/smiley_face.png"))
		assert(false);
	if (!gdata.deadHead.loadFromFile("data/sad_face.png"))
		assert(false);
	if (!gdata.font.loadFromFile("data/fonts/comic.ttf"))
		assert(false);

	InitialiseText(gdata.tlib);
	LoadText(gdata.tlib, "data/bible.txt");
	ParseText(gdata.tlib);
	gdata.score = GC::START_SCORE;
	gdata.level = 0;
	gdata.playerGuess.hints = GC::MAX_HINTS;
	gdata.playerGuess.nextHintLevel = GC::FIRST_HINT;
	Reset(gdata);
}

//player wants to spend a hint, so look at the word
//they are trying to guess and give them a missing letter
char GetHint(GameData& gdata)
{
	char key = 0;
	if (gdata.playerGuess.hints > 0)
	{
		int lidx = 0;
		char letters[GC::MAX_WORD_LENGTH];
		for (size_t i = 0; i < gdata.curWord.length(); ++i)
		{
			char letter = gdata.curWord.c_str()[i];
			bool found = false;
			for (int ii = 0; ii < GC::MAX_WORD_LENGTH; ++ii)
				if (gdata.playerGuess.word[ii] == letter)
					found = true;
			if (!found)
				letters[lidx++] = letter;
		}
		if (lidx > 0)
		{
			key = (lidx == 1) ? letters[0] : letters[GetRandomRange(0, lidx - 1)];
			gdata.playerGuess.hints--;
			gdata.score -= GC::GOODLETTER_SCORE;//don't let them have points for a hint
		}
	}
	return key;
}

/*
the player suggested a letter, see if it's a good match for any letters in the word thet
are trying to guess.
gdata - game data
key - the letter they just suggested
correct - how many letters in the target word does it match
lettersDone - total how many we have right so far
returns - true if a repeat
*/
bool CheckForMatch(GameData& gdata, char key, int& correct, int& lettersDone)
{
	bool repeat = false;
	for (size_t i = 0; i < gdata.curWord.length(); ++i)
	{
		if (gdata.curWord[i] == key)
		{
			if (gdata.playerGuess.word[i] != key)
			{
				//found one!
				correct++;
				gdata.playerGuess.word[i] = key;
				gdata.playerGuess.numTries++;
				if (gdata.playerGuess.numFails > 0)
					gdata.playerGuess.numFails--;
			}
			else
				repeat = true;
		}
		//how many have we got in total
		if (gdata.curWord[i] == gdata.playerGuess.word[i])
			lettersDone++;
	}
	return repeat;
}

/*
Player guess wrong, but check in case it's a repeat, we don't want to penalise them twice
Keep a record of the failed attempt in the fails array
gdata - game data
repeat - have we seen it before
key - their guess
*/
void BadMatch(GameData& gdata, int repeat, char key)
{
	//bad press detected
	//have we already got it?
	int i = 0;
	while (i < GC::NUM_ALPHABET && !repeat && isalpha(gdata.playerGuess.fails[i]))
	{
		if (key == gdata.playerGuess.fails[i])
			repeat = true;
		++i;
	}

	//sort the letter into the 'done' pile
	if (!repeat)
	{
		i = 0;
		bool found = false;
		while (i < GC::NUM_ALPHABET && !found && isalpha(gdata.playerGuess.fails[i]))
		{
			if (key < gdata.playerGuess.fails[i])
			{
				for (int ii = GC::NUM_ALPHABET - 1; ii > i; --ii)
					gdata.playerGuess.fails[ii] = gdata.playerGuess.fails[ii - 1];
				gdata.playerGuess.fails[i] = key;
				gdata.playerGuess.numTries++;
				found = true;
			}
			++i;
		}
		if (!found)//add it to the end?
		{
			gdata.playerGuess.fails[i] = key;
			gdata.playerGuess.numTries++;
		}
		gdata.score += GC::BADLETTER_SCORE;
		gdata.playerGuess.numFails++;
	}
}

/*
manage the game, check new letter guesses, update the scores, watch
for the game begin lost and work out when to trigger a new level
*/
void UpdateGame(GameData& gdata, char key)
{
	int correct = 0, lettersDone = 0;
	bool repeat = CheckForMatch(gdata, key, correct, lettersDone);
	if (correct > 0)
		//if they got some letters correct then score boost
		gdata.score += correct * GC::GOODLETTER_SCORE;
	else if (!repeat)
		BadMatch(gdata, repeat, key);

	//is the level complete?
	if (lettersDone == gdata.curWord.length())
		gdata.mode = GC::GameMode::LEVEL_WON;
	else if (gdata.score <= 0 || gdata.playerGuess.numFails > 7)
	{
		//game over?
		gdata.score = 0;
		gdata.mode = GC::GameMode::LOST;
	}
}

//get the player's guess so far and format for display
string GetPlayerGuess(GameData& gdata)
{
	string txt;
	for (size_t i = 0; i < gdata.curWord.length(); ++i)
	{
		txt += gdata.playerGuess.word[i];
		txt += " ";
	}
	return txt;
}

//Explain what's happening
void DrawGame(sf::RenderWindow& window, GameData& gdata)
{
	sf::Text txt;
	txt.setFont(gdata.font);

	stringstream ss;
	ss << "Tries:" << gdata.playerGuess.numTries;
	txt.setString(ss.str());
	txt.setPosition(sf::Vector2f(100, 250));
	txt.setFillColor(sf::Color::White);
	txt.setOutlineColor(sf::Color::Black);
	txt.setCharacterSize(30);
	window.draw(txt);
	ss.str("");
	ss << " Level:" << gdata.level + 1;
	txt.setPosition(sf::Vector2f(300, 250));
	txt.setString(ss.str());
	window.draw(txt);

	ss.str("");
	ss << "Target word: " << GetPlayerGuess(gdata);
	txt.setPosition(sf::Vector2f(100, 350));
	txt.setString(ss.str());
	window.draw(txt);

	ss.str("");
	ss << "Fails: " << gdata.playerGuess.fails;
	txt.setPosition(sf::Vector2f(100, 450));
	txt.setCharacterSize(20);
	txt.setString(ss.str());
	window.draw(txt);

	ss.str("");
	ss << "Score: " << gdata.score;
	txt.setPosition(sf::Vector2f(100, 550));
	txt.setString(ss.str());
	window.draw(txt);

	ss.str("");
	ss << "Hints remaining (press ENTER): " << gdata.playerGuess.hints << ", bonus hint at level " << gdata.playerGuess.nextHintLevel + 1;
	txt.setPosition(sf::Vector2f(100, 650));
	txt.setString(ss.str());
	window.draw(txt);

}


void DrawTitle(sf::RenderWindow& window, GameData& gdata)
{
	DrawGallows(window, gdata);

	//instantiate and draw a sprite
	sf::Sprite spr(gdata.wood);
	spr.setOrigin(gdata.wood.getSize().x / 2.f, gdata.wood.getSize().y / 2.f);
	spr.setPosition(GC::SCREEN_RES.x / 2.f, GC::SCREEN_RES.y * 0.2f);
	spr.setScale(Rescale(GC::SCREEN_RES, Dim2Df{ 0.9f,0.2f }, spr.getTextureRect()));
	window.draw(spr);

	sf::Text txt("Biblical Hangman", gdata.font, 90);
	txt.setFillColor(sf::Color::Blue);
	txt.setOutlineColor(sf::Color::Black);
	txt.setOutlineThickness(1);
	txt.setPosition(sf::Vector2f(100, 100));
	window.draw(txt);
}

void UpdateDrawIntro(sf::RenderWindow& window, GameData& gdata, char key)
{
	sf::Text txt("Welcome to Hangman, guess the word, press space to start.\n\nIf your score hits zero it's game over.\nIf your man is hanged it's game over.\nPress ENTER to get a free hint.\nCan you make it to level 10?", gdata.font, 30);
	txt.setPosition(sf::Vector2f(100, 250));
	txt.setFillColor(sf::Color::White);
	txt.setOutlineColor(sf::Color::Black);
	window.draw(txt);
	if (key == GC::SPACE)
		gdata.mode = GC::GameMode::PLAY;
}

void UpdateDrawWonLevel(sf::RenderWindow& window, GameData& gdata, char key)
{
	sf::Text txt("Hold tight for the next level!\n Press space", gdata.font, 30);
	txt.setPosition(sf::Vector2f(350, 500));
	txt.setFillColor(sf::Color::White);
	txt.setOutlineColor(sf::Color::Black);
	window.draw(txt);
	DrawGame(window, gdata);
	if (key == GC::SPACE)
		NextLevel(gdata);
}

void UpdateDrawGame(sf::RenderWindow& window, GameData& gdata, char key)
{
	if (key != 0)
		UpdateGame(gdata, key);
	DrawGame(window, gdata);
}

void UpdateDrawWon(sf::RenderWindow& window, GameData& gdata, char key)
{
	sf::Text txt("Game over, you win\nPress space", gdata.font, 30);
	txt.setPosition(sf::Vector2f(350, 500));
	txt.setFillColor(sf::Color::White);
	txt.setOutlineColor(sf::Color::Black);
	window.draw(txt);
	DrawGame(window, gdata);
	if (key == GC::SPACE)
		window.close();
}

void UpdateDrawLost(sf::RenderWindow& window, GameData& gdata, char key)
{
	sf::Text txt("Game over, you lose\nPress space to restart\nPress q to quit", gdata.font, 30);
	txt.setPosition(sf::Vector2f(350, 500));
	txt.setFillColor(sf::Color::White);
	txt.setOutlineColor(sf::Color::Black);
	window.draw(txt);
	DrawGame(window, gdata);
	if (key == GC::SPACE)
	{
		gdata.mode = GC::GameMode::INTRO;
		Initialise(gdata);
	}
	else if (key == 'q' || key == 'Q')
		window.close();
}