#pragma once
#include <string>
#include "SFML/Graphics.hpp"

#include "GameConsts.h"

/*
* After many versions, this is the final hangman game!
*/


//a collection of words all the same length
struct TextGrp
{
	int length = 0;	//all words are this long
	int nWords = 0;	//how many did we get
	int startIds[GC::MAX_WORDS]; //the id of the first letter of each word inside the main data buffer
};

//a set of TextGrps covering a range of lengths
struct TextLib
{
	TextGrp wordGrps[GC::WORD_LENGTHS]; //each text group
	char data[GC::MAX_CHARS];			//the original massive data buffer
	unsigned int numChars = 0;			//how much data we got
};

//everything related to the player trying to guess a letter
struct PlayerGuess
{
	char word[GC::MAX_WORD_LENGTH];	//what they've got so far
	int numTries = 0;				//how many attempts they've had
	int numFails = 0;				//how many times they got it wrong (ignore repeats)
	int hints = GC::MAX_HINTS;		//hints for when they get stuck
	int nextHintLevel = 0;			//track how often they get a new hint
	char fails[GC::NUM_ALPHABET];	//remember all the letters they got wrong
};

//all the game variables
struct GameData
{
	GC::GameMode mode = GC::GameMode::INTRO;	//wha tmode is the game in
	TextLib tlib;								//the text library
	sf::Texture wood, head, deadHead;			//textures
	sf::Font font;								//main font
	PlayerGuess playerGuess;	//player's current guess					
	std::string curWord;		//the word they are trying to guess
	int level = 0;				//current level
	int score = 0;				//overall score	
};

//draw the hanged man, a bit a time as they make mistakes
//getting a right answer undoes the last bit
void DrawGallows(sf::RenderWindow& window, GameData& gdata);

//set everything at the start (or a restart)
void Initialise(GameData& gdata);

//find a missing letter and give it to them
char GetHint(GameData& gdata);

//titles are always drawn
void DrawTitle(sf::RenderWindow& window, GameData& gdata);

//update the game depending on what mode they are in
void UpdateDrawIntro(sf::RenderWindow& window, GameData& gdata, char key);
void UpdateDrawWonLevel(sf::RenderWindow& window, GameData& gdata, char key);
void UpdateDrawGame(sf::RenderWindow& window, GameData& gdata, char key);
void UpdateDrawWon(sf::RenderWindow& window, GameData& gdata, char key);
void UpdateDrawLost(sf::RenderWindow& window, GameData& gdata, char key);



