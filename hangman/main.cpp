#include <assert.h>

#include "Game.h"
#include "RandomUtils.h"

using namespace std;


//why is this global? You know why!
GameData gdata;

/*
* After many versions, this is the final hangman game!
*/
int main()
{
	// Create the main window
	sf::RenderWindow window(sf::VideoMode(GC::SCREEN_RES.x, GC::SCREEN_RES.y), "Hangman");
	Seed();
	Initialise(gdata);
	char key = 0;
	// Start the game loop 
	while (window.isOpen())
	{
		// Process events
		char key = 0;
		sf::Event event;
		while (window.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window.close();
				break;
			case sf::Event::TextEntered:
				if (event.text.unicode == GC::ESCAPE_KEY)
				{
					window.close();
				}
				else
				{
					key = event.text.unicode;
					if (gdata.mode == GC::GameMode::PLAY)
					{
						key = toupper(key);
						if (key == GC::ENTER)
							key = GetHint(gdata);
						if (!isalpha(key))
							key = 0;
					}
				}
			}
		}  

		// Clear screen
		window.clear();
		DrawTitle(window,gdata);



		switch (gdata.mode)
		{
		case GC::GameMode::INTRO:
			UpdateDrawIntro(window, gdata, key);
			break;
		case GC::GameMode::LEVEL_WON:
			UpdateDrawWonLevel(window, gdata, key);
			break;
		case GC::GameMode::PLAY:
			UpdateDrawGame(window, gdata, key);
			break;
		case GC::GameMode::WON:
			UpdateDrawWon(window, gdata, key);
			break;
		case GC::GameMode::LOST:
			UpdateDrawLost(window, gdata, key);
			break;
		}



		// Update the window
		window.display();
	}

	return EXIT_SUCCESS;
}
