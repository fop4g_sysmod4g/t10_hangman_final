/*
**A simple library of random numbers routines  
*/

#ifndef RandomUtilsH
#define RandomUtilsH

#include <stdlib.h>		//for rand and srand
#include <ctime>		//for time used in random number routines
#include <cassert>		//for assert

//Seed() - seed the random number generator from current system time 
void Seed(int seed=-1);

//Random(max) - produce a random number in range [1..max]
//pre-condition: max > 0
int Random(int max); 

/*
Get a random number between the two limits, the random
number could actually be min or max or something inbetween
*/
float GetRandomRange(float min, float max);
int GetRandomRange(int min, int max);

#endif